const mysql = require('mysql');
const express = require('express');
let app = express();
const router = require('./src/router');
const connectToDatabase = require('./src/databaseConnection');

app.use(express.json());
app.use(router);

require('dotenv').config();

app.listen(3000, async () => {
    try {
        await connectToDatabase();
        console.log('express is running');
    } catch (err) {
        console.log({ err })
    }
})