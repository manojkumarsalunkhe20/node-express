const { check } = require('express-validator');

const schemaValidation = [
    check('emp_name', "name field cannot not be blank").notEmpty(),
    check('emp_age', "age field cannot be blank").notEmpty(),
    check('emp_dept', "dept field cannot be blank").notEmpty()
]

module.exports = schemaValidation
