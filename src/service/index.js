const repostiory = require('../repositories/index');

const getEmployeesService = () => {
    const employees = repostiory.getEmployees();
    return employees
}

const getSingleEmployeeService = (id) => {
    const employee = repostiory.getSingleEmployeeRepository(id);
    return employee
}

const postEmployeeService = (body) => {
    const result = repostiory.postEmployeeRepository(body);
    return result
}

const putEmployeeService = (body,id) => {
    const result = repostiory.putEmployeeRepository(body,id);
    return result
}

module.exports = { getEmployeesService, getSingleEmployeeService, postEmployeeService, putEmployeeService }