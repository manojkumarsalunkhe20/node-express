const service = require('../service/index')

const getAllEmployees = async (req, res, next) => {
    // const {id} = req.params
    try {
        const employees = await service.getEmployeesService();
        return res.json(employees);
    } catch (err) {
        console.log(err.message)
    }
}

//get single employee
const getSingleEmployee = async (req, res, next) => {
    const { id } = req.params
    console.log(id)
    try {
        const employees = await service.getSingleEmployeeService(id);
        return res.json(employees);
    } catch (err) {
        console.log(err.message)
    }
}

// post employee 

const postEmployee = async (req, res) => {
    let body = req.body
    try {
        const result = await service.postEmployeeService(body)
        return res.json(result)
    } catch (err) {
        console.log(err.message)
    }
}

const putEmployee = async (req, res) => {
    let body = req.body, id = req.params.id
    try {
        const result = await service.putEmployeeService(body, id)
        return res.json(result)
    } catch (err) {
        console.log(err.message)
    }
}

module.exports = { getAllEmployees, getSingleEmployee, postEmployee, putEmployee }