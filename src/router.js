const express = require('express');
const router = express.Router();
const employee = require('./controllers/index')
const schemaValidation = require('./validators/index')
const validation = require('./utilities/validation')

router.get('/employee', employee.getAllEmployees);
router.get('/employee/:id', employee.getSingleEmployee);
router.post('/employee', schemaValidation, validation, employee.postEmployee);
router.put('/employee/:id', employee.putEmployee)

module.exports = router