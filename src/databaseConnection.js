const mysql = require('mysql-await');
require('dotenv').config();

let HOST = process.env.HOST
let PORT = process.env.PORT
let USER = process.env.USER
let PASSWORD = process.env.PASSWORD
let DATABASE = process.env.DATABASE

const dbConfig = {
    host: HOST,
    port: PORT,  //MySql (databse) port
    user: USER,
    password: PASSWORD,
    database: DATABASE
}

const connectToDatabase = () => {
    const mysqlConnection = mysql.createConnection(dbConfig);

    mysqlConnection.connect((err) => {
        if (!err)
            console.log('DB connection succeded');
        else
            console.log('DB connection failed : ', JSON.stringify(err));
    });

    return mysqlConnection
}

module.exports = connectToDatabase




