const connectToDatabase = require("../databaseConnection");

const getEmployees = async () => {
    try {
        const employees = await connectToDatabase().awaitQuery("SELECT * FROM testdb1.employee");
        // console.log(employees)
        return employees
    } catch (err) {
        // console.log(err)
        return err.message
    }
}

const getSingleEmployeeRepository = async (id) => {
    try {
        const employee = await connectToDatabase().awaitQuery('SELECT * FROM testdb1.employee WHERE emp_id = ?', id);
        // console.log(employee)
        return employee
    } catch (err) {
        // console.log(err)
        return err.message
    }
}

const postEmployeeRepository = async (body) => {
    try {
        const result = await connectToDatabase().awaitQuery(`INSERT INTO testdb1.employee VALUES (?,?,?,?)`,
            [body.emp_id, body.emp_name, body.emp_age, body.emp_dept])
        return result
    } catch (err) {
        return err.message
    }
}

const putEmployeeRepository = async (body, id) => {
    try {
        const result = await connectToDatabase().awaitQuery('UPDATE testdb1.employee SET emp_id = ? ,emp_name = ?,emp_age = ? ,emp_dept = ? WHERE emp_id = ?',
            [body.emp_id, body.emp_name, body.emp_age, body.emp_dept, id])
        return result
    } catch (err) {
        return err.message
    }
}

module.exports = { getEmployees, getSingleEmployeeRepository, postEmployeeRepository, putEmployeeRepository }
